# Video frame seeker

The app looks for a specific frame in a video file. Written in Cpp.

Для сборки проекта надо скачать библиотеки https://github.com/opencv/opencv/releases/download/4.2.0/opencv-4.2.0-vc14_vc15.exe
и подключить их в настройках компоновщика. Добавить в PATH путь к dll.

Для запуска предварительно собранного проекта достаточно закинуть файл opencv_world420d.dll в папку с бинарником.
Запуск батником run.bat