/**
* @file videocapture_starter.cpp
* @brief A starter sample for using OpenCV VideoCapture with capture devices, video files or image sequences
* easy as CV_PI right?
*
*  Created on: Nov 23, 2010
*      Author: Ethan Rublee
*
*  Modified on: April 17, 2013
*      Author: Kevin Hughes
*/

#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <math.h>
#include <iostream>
#include <stdio.h>

using namespace cv;
using namespace std;

//hide the local functions in an anon namespace
namespace {
	void help(char** av) {
		cout << "The program captures frames from a video file, and compares them with test image." << endl
			<< "Usage:\n" << av[0] << " <video file,image>" << endl;
	}

	int process(VideoCapture& captureFile, Mat& originalImage) {

		Mat frame;
		int frameCounter = 0, sFrame = -1;
		auto fps = captureFile.get(CAP_PROP_FPS);
		float min = 999;
		for (;;) {
			captureFile >> frame;
			if (frame.empty())
				break;
			float summ_total = 0;
			for (int i = 0; i < frame.rows; ++i)
				for (int j = 0; j < frame.cols; ++j)
				{
					Point3_<uchar>* framePixel = frame.ptr<Point3_<uchar> >(i, j);
					Point3_<uchar>* imagePixel = originalImage.ptr<Point3_<uchar> >(i, j);
					float summ = abs(framePixel->x - imagePixel->x);//bgr
					summ += abs(framePixel->y - imagePixel->y);
					summ += abs(framePixel->z - imagePixel->z);
					summ /= 3;
					summ_total += summ;
				}
			summ_total /= frame.rows * frame.cols;

			cout << "frame " << ++frameCounter << " similarity is " << summ_total << endl;
			if (summ_total < min)
			{
				min = summ_total; sFrame = frameCounter;
			}
		}
		cout << "total frames processed " << frameCounter
			<< " at frame rate " << fps
			<< " FPS. Most similar is frame # " << sFrame << endl;
		return 0;
	}
}

int main(int ac, char** av) {
	cv::CommandLineParser parser(ac, av, "{help h||}{@input||}");
	if (parser.has("help"))
	{
		help(av);
		return 0;
	}
	std::string arg = av[1];
	if (arg.empty()) {
		help(av);
		return 1;
	}
	VideoCapture capture(arg); //open  video file 
	//if (!capture.isOpened()) //if this fails, try to open as a video camera, through the use of an integer param
	  //  capture.open(atoi(arg.c_str()));
	if (!capture.isOpened()) {
		cerr << "Failed to open the video file !\n" << endl;
		help(av);
		return 1;
	}

	Mat I = imread(av[2], IMREAD_COLOR);

	if (I.empty())
	{
		cout << "The test image  could not be loaded." << endl;
		return -1;
	}
	return process(capture, I);
}
